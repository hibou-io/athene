ARG NODE_VERSION=18.19

FROM node:${NODE_VERSION}
RUN apt-get update && \
    apt-get install -y \
      git \
      rsync \
      libsecret-1-dev

RUN mkdir /opt/project && \
    mkdir /opt/athene && \
    chown -R node:node /opt/project /opt/athene && \
    chmod -R 777 /opt/project /opt/athene

WORKDIR /opt/athene
ADD entrypoint.sh ./entrypoint.sh
ADD package.json ./package.json
ADD .theia ./.theia
ADD plugins ./plugins

ARG GITHUB_TOKEN
RUN chown -R 777 /opt/athene && \
    yarn --pure-lockfile && \
    yarn install && \
    yarn download:plugins && \
    yarn build:prod && \
    yarn cache clean && \
    ln -s /opt/athene/.theia /opt/project/.theia && \
    chown -R node:node /opt

USER node

ENV HOME /opt/athene
WORKDIR /opt/project

EXPOSE 3000
ENV SHELL=/bin/bash \
    THEIA_DEFAULT_PLUGINS=local-dir:/opt/athene/plugins
ENV USE_LOCAL_GIT true
ENTRYPOINT [ "/opt/athene/entrypoint.sh" ]

