#!/bin/bash

set -e

#    DEV_MODE=exclusive
#      Will start the Theia IDE in the foreground, you can then start Odoo from a terminal.
#    DEV_MODE=1
#      Will start the Theia IDE in the background, regular Odoo commands will still work.
#      Note that in Theia you can re-start Odoo e.g.
#        `kill -s SIGHUP 1` to reload/restart Odoo
#        `kill -s SIGQUIT 1` to cause Odoo to dump stacktrace in standard out
#      Note that with Odoo running in the foreground, killing Odoo will kill the container.
#    DEV_MODE=
#      Unset to not use Theia at all.
#    
#    DEV_MODE_PATH=/opt/odoo/addons
#      To change the path to start Theia in, useful to get git working.

# Copy our own in so that we start with defaults.
if [ "$DEV_MODE_PATH" != "" ] && [ -z "$(ls -A $DEV_MODE_PATH/.theia)" ]
then
   cp -R /opt/athene/.theia $DEV_MODE_PATH
fi

if [ "$DEV_MODE_PATH" == "" ]
then
  export DEV_MODE_PATH=/opt/project
  # Note that if you define the path, you also define the mode.
  if [ "$DEV_MODE" == "" ]
  then
    export DEV_MODE=exclusive
  fi
fi

# setup development IDE
if [ "$DEV_MODE" == "exclusive" ]
then
    cd /opt/athene
    # before calling node, we clear the env variable to make this non-reentrant
    export DEV_MODE=
    exec node /opt/athene/src-gen/backend/main.js $DEV_MODE_PATH --hostname=0.0.0.0
elif [ "$DEV_MODE" != "" ]
then
    cd /opt/athene
    # before calling node, we clear the env variable to make this non-reentrant
    export DEV_MODE=
    node /opt/athene/src-gen/backend/main.js $DEV_MODE_PATH --hostname=0.0.0.0 &
fi


